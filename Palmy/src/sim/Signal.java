package sim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.omg.PortableInterceptor.ACTIVE;

public class Signal {

	public int gid = 0;
	private static int total_number;
	public String name;
	public int datatype;
	private boolean status = S2U.ACTIVE;
	private Block srcblk;
	private String srcport;
	
	/* Constructor with two arguments*/
	public Signal(Line line) 
	{
		this.gid = ++total_number;
		this.name = String.format("signal_%d", this.gid);
		this.srcblk = line.getSrcBlock();
		this.datatype = srcblk.getOutDataType();
		
		this.srcport = line.getSrcPort();
	}
	
	/* Id */
	public int getGId()
	{
		return this.gid;
	}
	
	/* Name */
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}
	
	/* Datatype */
	public int getDataType()
	{
		return this.datatype;
	}
	
	/* Block */
	public Block getSrcBlock()
	{
		return this.srcblk;
	}
	/* Block */
	public String getSrcPort()
	{
		return this.srcport;
	}
	
	
}
