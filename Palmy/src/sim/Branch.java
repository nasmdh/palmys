package sim;

import java.util.HashMap;

public class Branch {

	private Line line;
	private Block dstblk;
	private String dstport;
	private HashMap<String, String> para_hm = new HashMap<String, String>();
	
	public Branch(Line line, HashMap<String, String> para_hm) 
	{
		this.line = line;
		/* Destiniation */
		this.dstblk = line.getParent().getBlock(para_hm.get("DstBlock"));
		this.dstport = para_hm.get("DstPort");
	}
	
	public HashMap<String, String> getParas()
	{
		return this.para_hm;
	}
	
	public Block getDstBlock()
	{
		return this.dstblk;
	}
	public String getDstPort()
	{
		return this.dstport;
	}
}
