package sim;

import java.util.*;


public class Block{

	private int gid = 0;
	private int lid = 0;
	private static int id_counter;
	
	/* main parameters */
	public String name;
	public String type;
	public String sample_time;
	public String port;
	
	/* input and output index of ports. The ports have global index that correspond to the signals' index. */
	private List<Integer> insignal_list = new ArrayList<Integer>();
	private List<Integer> outsignal_list = new ArrayList<Integer>();
	
	/* Datatype */
	public int in_datatype;
	public int out_datatype;
	public String para_datatype_str;
	public String out_datatype_str;
	
	/* Relations with its system node*/
	private Node ref_node;
	private Node parent;
	
	/* Execution order */
	private int so_number;
	
	/* its status */
	private boolean status = S2U.ACTIVE;
	
	/* Derived parameters */
	private String unique_name;
	private String abs_name;
	private int abs_name_hashed;
	private String abs_name_so;
	
	private String compiled_sample_time;
	
	HashMap<String,String> para_hm = new HashMap<String,String>();
	
	private List<Line> inline_list = new ArrayList<Line>();
	private List<Line> outline_list = new ArrayList<Line>();
	
	/* Constructors */
	public Block() 
	{
		this.gid = ++this.id_counter;
	}
	
	public Block(Node parent, HashMap<String,String> hm) 
	{
		this.gid = ++this.id_counter;
		this.parent = parent;
		this.para_hm.putAll(hm);
		this.setMainParas();
	}
	
	public Block(HashMap<String,String> p) 
	{
		this.gid = ++this.id_counter;
		p.putAll(this.para_hm);
	//type.replaceAll("^[0-9]+", "");
	//type.replaceAll("[ \t\n\r\f]+", "");
	}
	
	/* id */
	public int getGId() { return this.gid;	}
	public int getLId()	{ return this.gid;	}
	
	/* Line */
	public void addInLine(Line l) {this.inline_list.add(l);}
	public void addOutLine(Line l) {this.outline_list.add(l);}
	public Line getInLine(int i) {  return this.inline_list.get(i); }
	public Line getOutLine(int i) { return this.outline_list.get(i); }
	public List<Line> getInLines() {  return this.inline_list; }
	public List<Line> getOutLines() { return this.outline_list; }
	//public void updateLine(Line l) {int i = this.outline_list.indexOf(l); this.outline_list.set(i, element)(index, element)}
	
	/* path */
	public String getAbsName()
	{
		return this.abs_name;
	}
	
	/* Sorted Order number */
	public int getSorderNumber() { return this.so_number; }
	public void setSorderNumber(int so_number) { this.so_number = so_number; }
	
	public Node getParent()
	{
		return this.parent;
	}

	public String getUniqueName()
	{
		return this.unique_name;
	}
	
	/* Set main parameters */
	private void setMainParas()
	{
		this.name = para_hm.get("Name");
		this.type = para_hm.get("BlockType");;
		
		String st;
		if((st = para_hm.get("SampleTime")) != null)
			this.sample_time = st;
		else if((st = para_hm.get("SystemSampleTime")) !=  null)
			this.sample_time = st;
		else
			this.sample_time = "0";// sample time not set
		
		if(this.sample_time.equals("inf"))
			this.compiled_sample_time = "0";
		else if(this.sample_time.equals("-1"))
			this.compiled_sample_time = "0";
		else
			this.compiled_sample_time = this.sample_time;

			
		// determine the port number 
		String tmp;
		  if((tmp = para_hm.get("Port")) == null)
			this.port = "1";
		  else
			  this.port = tmp;
		  
		// set the absolute block name  
		  this.abs_name = String.format("%s/%s", this.parent.abs_path, this.name);
		  this.abs_name_so = String.format("%s/%s", this.parent.abs_path_so, this.name.replace("/", "//")).replace("\\n", " ");
		  this.abs_name_hashed = this.abs_name_so.trim().hashCode();
		 // System.out.println(abs_name);//.replace("\\n", " ")
		  
		  calcUniqueName();
		  
		  // datatype
		  this.para_datatype_str = para_hm.get("ParamDataTypeStr");
		  this.out_datatype_str = para_hm.get("OutDataTypeStr");
		  
		  if( this.para_datatype_str == null)
			  this.para_datatype_str = "";
		  if( this.out_datatype_str == null)
			  this.out_datatype_str = "";
		  
		  if(this.type.equals("Mux"))
		  {
			  this.in_datatype = S2U.SCALAR;
			  this.out_datatype = S2U.VECTOR;
		  }else if(this.type.equals("Demux"))
		  {
			  this.in_datatype = S2U.VECTOR;
			  this.out_datatype = S2U.SCALAR;
		  }
		  else
		  {
			  this.in_datatype = S2U.SCALAR;
			  this.out_datatype = S2U.SCALAR;
		  }
			  
	}
	
	public String getAbsNameSortedOrderNumber() { return this.abs_name_so;}
	public int getAbsNameHashed() { return this.abs_name_hashed; }
	
	public void setCompiledSampleTime(String st)
	{
		this.compiled_sample_time = st;
	}
	
	/* Datatype */
	public int getInDataType()
	{
		return this.in_datatype;
	}
	
	public int getOutDataType()
	{
		return this.out_datatype;
	}
	
	void calcUniqueName()
	{
		// set the block unique name
		  String sysed_name = "", hashed_name;
		 // sysed_name = String.format("node%d_%s", this.parent.getId(), this.name.replaceAll("[ \"\t\n\r\f:/-]+", ""));
		  //sysed_name = sysed_name.replace("\\n", "");
		  
		  int hc = this.abs_name.hashCode() & 0xfffffff;
		  hashed_name = String.format("%d", hc);
		  
		  switch (S2U.BLOCK_NAMING)
		  {
		  case 0://hash code
			  this.unique_name = hashed_name;
			  break;
		  case 1:// using the node id as identifier
			  this.unique_name = sysed_name;
			  break;
		  case 2:
		  default:
			  System.out.println("BLOCK_NAMING:  allowed values are '0 - hased_name, 1-sysed_name'");
		  }
		  
	}

	/* Block status */
	public void setStatus(boolean status) 
	{ 
		this.status = status;
	}
	
	public boolean getStatus() 
	{
		return this.status;
	}
	
	public String getPort()
	{
		return this.port;
	}
	
	/* Reference node */
	public void setRefNode(Node child_node)
	{
		this.ref_node = child_node;
	}
	public Node getRefNode()
	{
		return this.ref_node;
		
	}
	
	public HashMap<String,String> getParas()
	{
		return this.para_hm;
	}
	
	/* Block main parameters */
	public String getName(){return this.name;}	
	public String getType(){return this.type;}
	public String getSampleTime(){return this.sample_time;}	
	public String getCompiledSampleTime(){return this.compiled_sample_time;}	
	
	/* Sorted Order Number */
	public void setSortedOrderNumber(int so){this.so_number = so;}
	public int getSortedOrderNumber(){ return this.so_number; }	
	
	
	public void mapIndexOfSignal()
	{
		this.outsignal_list.set(0, this.insignal_list.get(0));
	}
	
	public int getIndexOfInSignal(int i)
	{
		return this.insignal_list.get(i);
	}
	public int getIndexOfOutSignal(int i)
	{
		return this.outsignal_list.get(i);
	}
	
	/* Add signal index */
	public void addIndexOfInSignal(int i)
	{	   
		this.insignal_list.add(i); 	  	
	}
	public void addIndexOfOutSignal(int i)
	{	   
		this.outsignal_list.add(i); 	  	
	}
	
	/* Get signals */
	public List<Signal> getInSignals(List<Signal> signal_list)
	{	   
		List<Signal> sl = new ArrayList<Signal>();
		for (Integer i : this.insignal_list) 
		{
			sl.add(signal_list.get(i));
		}
		return sl; 	  	
	}
	public List<Signal> getOutSignals(List<Signal> signal_list)
	{	   
		List<Signal> sl = new ArrayList<Signal>();
		for (Integer i : this.outsignal_list) 
		{
			sl.add(signal_list.get(i));
		}
		return sl; 	  	
	}
	
	public List<Integer> getIndexOfInSignals()
	{	   
		
		return this.insignal_list; 	  	
	}
	public List<Integer> getIndexOfOutSignals()
	{	   
		
		return this.outsignal_list; 	  	
	}
	
	/* Get a signal */
	public Signal getInSignal(List<Signal> signal_list, int index)
	{	   
		return signal_list.get(insignal_list.get(index)); 	  	
	}
	public Signal getOutSignal(List<Signal> signal_list, int index)
	{	   
		return signal_list.get(outsignal_list.get(index)); 	  	
	}
	
	
	/* Flatten a SubSystem block */
	public Block FlattenSubSystemBlock(String port, int type)
	{
		if(!this.type.equals("SubSystem"))
		{
			System.out.format("Error:FlattenSubSystemBlock::Block - Block is not a Subsystem ( Block name: %s, type: %s) %n", this.name, this.type);
		}else
		{	
			switch(type)
			{
			  case S2U.SOURCE_BLOCK:
	
			  for(Block blk : this.ref_node.getBlocks())
			  {
					if(blk.getType().equals("Outport"))
				    {
						if(blk.getPort().equals(port))
				    	{
							S2ULog.debug(String.format("Mapping: %s:%s == > %s:%s%n",
							this.name, port, blk.getName(),  blk.getPort()));
	
							//disconnect the SubSystem block by dereferencing it the input port, ref_blk
							return blk;
						}
				      }
			  }
			  break;
			  case S2U.DESTINATION_BLOCK:
	
		 	  	  for(Block blk : this.ref_node.getBlocks())
				  {
				    if(blk.getType().equals("Inport"))
				    {//System.out.format("Subsystem port: %s, Inport: %s%n", port, blk.getPort());
						if(blk.getPort().equals(port))
				    	{
							S2ULog.debug(String.format("Mapping: %s:%s == > %s:%s%n",
							this.name, port, blk.getName(),  blk.getPort()));
	
							//disconnect the SubSystem block by dereferencing it the input port, ref_blk
							return blk;
						}
				     }
				    
				  }//System.out.format("Port didnt match!!! %s\t%s%n", this.abs_name, this.ref_node.abs_path);
				  break;
			  default:
				  S2ULog.debug("unknown error: flattening the model");
			}
		}
		return null;
	}

}
