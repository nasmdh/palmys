package sim;

public interface S2U {
	public final static boolean ACTIVE = true;
	public final static boolean PASSIVE = false;
	public final static char OUTLINE_CHAR = '=';
	
	public final static int BLOCK_NAMING = 0; // 0 -hashed name, 1 - sysed name(not implemented)
	public final static boolean CREATE_UPPAAL_XML_ELEMENTS = false;
	public final static boolean CREATE_UPPAAL_XML_FILE = false;
	
	/* Signal datatype */
	public final static int SCALAR = 1;
	public final static int VECTOR = 2;
	

	/* Block optimization */
	public final static int SOURCE_BLOCK = 0;
	public final static int DESTINATION_BLOCK = 1;
	
	
	/* Logging */
	public final static String LOG_FILE_PATH = "./output/debug.log";
	public final static char DEBUG_LEVEL = 0;
	public final static boolean DEBUGGING = true;
	public final static boolean LOGGING = false;
	
	/* Optimiation */
	public final static boolean OPTIMIZATION = true;
	public final static boolean OPTIMIZATION_CONSTANT_BLOCK = true;
	
}
