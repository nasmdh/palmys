package sim;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
 
public class Node 
{
	
	private int id;
	private static int number_of_nodes;
	
	/* Main parameters */
	private String name;
	private final Node parent;
	private String atomic;
	private String sample_time;
	
	private HashMap<String, String> para_hm =  new HashMap<String, String>();
	
	/* Statistics */
	public int blk_count;
	public int line_count;
	public  int ann_count;
	
	/* Derived parameters */
	
	public String abs_path;
	public String abs_path_so;
	private String compiled_sample_time;
	public int level = 0;
	public String indent="";
	
	/* Children system nodes */
	private List<Block> blk_list =  new ArrayList<Block>(); 
	/*Children atomic blocks - leaves */
	private final List<Node> children = new ArrayList<Node>();
	/* Signals */
	private List<String> signal_list = new ArrayList<String>();

 public Node(Node parent, String name) 
{
	 this.id = number_of_nodes++;
	 
	 /* Set main parameters */
	 this.name = name;
	 this.atomic = "on";
	 
	 /* Set derived parameters */
	 this.parent = parent;
	 this.level = 0;
	 this.abs_path = this.name = name;
	 this.abs_path_so = this.name.replace("/", "//");
	 this.compiled_sample_time = "-1";
	 
}
 
 public Node (Node parent, HashMap<String, String> hm) 
 {
	 this.id = number_of_nodes++;
	 this.parent = parent;
	 this.para_hm.putAll(hm);
	  
	 /* Set main parameters */
	 setMainParas();
	  
	 /* Set derived parameters */
	 setDerivedParas();
	 this.level = parent.level +  1;
	  
	 setIndent(S2U.OUTLINE_CHAR);
 }
 
 /* Set derived parameters */
 private void setDerivedParas()
 {
	 this.abs_path = String.format("%s/%s",parent.abs_path, this.name);
	 this.abs_path_so = String.format("%s/%s",parent.abs_path_so, this.name.replace("/", "//"));
	 compileSampleTime();
 }
 
 /* Set main parameter */
 private void setMainParas()
 {
	 this.name = para_hm.get("Name");
	 this.atomic = para_hm.get("TreatAsAtomicUnit");
	 this.sample_time = para_hm.get("SystemSampleTime");
 }
 
 /* Inherit sample time from parent if its sample time is -1 */
 public void compileSampleTime()
 {
  	if(this.sample_time.equals("-1"))
  		this.compiled_sample_time = parent.getCompiledSampleTime();
  	else
  		this.compiled_sample_time = "-1";
  	
  		
 }
 
 public String getCompiledSampleTime()
 {
	 return this.compiled_sample_time;
 }
 
 /* set unique id */
 private void setIndent(char c)
 {
	 char[] data = new char[level];
	   Arrays.fill(data, c);
	   this.indent = (new String(data)).concat(">");
	 
 }
  
 /* Node (system) atomic parameter, inherited from the SubSystem block  */
 public void setAtomic(String atomic)
 {
	 this.atomic = atomic;
 }
 public String getAtomic()
 {
	 return this.atomic;
 }
 
 /* sample time parameter , inherited from the SubSystem block */
 public void setSampleTime(String sample_time)
 {
	 this.sample_time = sample_time;
 }
 public String getSampleTime()
 {
	 return this.sample_time;
 }
 
 public int getId() {
  return id;
 }
 
 public String getName() {
	  return this.name;
 }
 
 public List<Node> getChildren() {
  return children;
 }
 
 public Node getParent() {
  return parent;
 }
 
 public void addBlock(Block blk)
 {
	 this.blk_list.add(blk);
 }

 public List<Block> getBlocks()
 {
	 return this.blk_list;
 }
 
 /* Print the system nodes */
 public void PrintModel(String parent) {
   /* add the subsystem name to the absolute path of the block only if
   the SubSystem block is atomic, in other words System element under the SubSystem block is atomic */
   String sys_name = "";
   
   if(getAtomic().equals("on"))
   {
     	System.out.println("Atomic!");
  		sys_name = parent.concat("/").concat(getName());

  		
  		for(Block b: getBlocks())
  		{
  		  if(b.getStatus() == S2U.ACTIVE)
  		  {
  		 // 	b.setCompiledSampleTime(getSampleTime());
			System.out.format("%s\t ( Type %s, SampleTime %s, CompiledSampleTime %s status %b)%n",
			sys_name.concat("/").concat(b.getName().replace("\\n", " ")), b.getType(),
			b.getSampleTime(), b.getCompiledSampleTime(), b.getStatus());
  		  }
  		}
    }else
    { 
		System.out.println("Not Atomic!");
		for(Block b: getBlocks())
		{
		  if(b.getStatus() == S2U.ACTIVE)
		  {
			sys_name = parent;
			System.out.format("%s\t ( Type %s, SampleTime %s, CompiledSampleTime %s status %b)%n",
			sys_name.concat("/").concat(b.getName()).replace("\\n", " "), b.getType(),
			b.getSampleTime(), b.getCompiledSampleTime(), b.getStatus());
		  }
		}
	}

  	for (Node each : getChildren())
  	{
  		 each.PrintModel(sys_name);
  	}
  
 }

 /* Print the system nodes */
 public void PrintModelTest(String parent) {
   /* add the subsystem name to the absolute path of the block only if
   the SubSystem block is atomic, in other words System element under the SubSystem block is atomic */
   String sys_name = "";
   
   if(getAtomic().equals("on"))
   {
     	System.out.println("Atomic!");
  		sys_name = parent.concat("/").concat(getName());

  		
  		for(Block b: getBlocks())
  		{
  		  if(b.getStatus() == S2U.ACTIVE)
  		  {
  		  //	b.setCompiledSampleTime(getSampleTime());
			System.out.format("%s\t ( Type %s, SampleTime %s, CompiledSampleTime %s status %b)%n",
			sys_name.concat("/").concat(b.getName()), b.getType(),
			b.getSampleTime(), b.getCompiledSampleTime(), b.getStatus());
  		  }
  		}
    }else
    { 
		System.out.println("Not Atomic!");
		for(Block b: getBlocks())
		{
		  if(b.getStatus() == S2U.ACTIVE)
		  {
			sys_name = parent.concat("/").concat(getName());
			
			System.out.format("%s\t ( Type %s, SampleTime %s, CompiledSampleTime %s status %b)%n",
			sys_name.concat("/").concat(b.getName()), b.getType(),
			b.getSampleTime(), b.getCompiledSampleTime(), b.getStatus());
		  }
		}
	}

  	for (Node each : getChildren())
  	{
  		 each.PrintModelTest(sys_name);
  	}
  
 }
 
 public Block getBlock(String name)
 {
	 for (Block blk : blk_list) 
	 {
		 //System.out.format("block name %s %s%n", blk.getName(), name); 
		if(blk.getName().equals(name))
		{
			//System.out.format("Found: %s%n", name);
			return blk;
		}
	}
	 System.out.format("Error: getBlock()::Node - Not found: %s%n", name);
	return null;
	 
 }
}