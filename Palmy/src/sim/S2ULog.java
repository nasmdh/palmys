package sim;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class S2ULog extends Model{
	
	private static String log_block_name = "";
	private static OutputStream fos;
	
	public S2ULog() {
		// TODO Auto-generated constructor stub
	}
	
	public S2ULog(int level, String file_path) 
	{		
		if(level == S2U.DEBUG_LEVEL)
		{
			// write to a log file
			 try {
				 fos = new FileOutputStream(new File(file_path));
		 	} catch (IOException e) {
				System.err.println("Fail (DEBUG LEVEL): Output file stream could not created!");
				e.printStackTrace();
			}
		}
		
	}
	
	/* write log to a file */
	 private static void writeToFile(String log, String file_name)
	 {
		 OutputStream fos;
		 
		// write to a log file
		 try {
			 fos = new FileOutputStream(new File(file_name));
			 byte[] b = log.getBytes();
			 
			 fos.write(b);
			 fos.close();
	 	} catch (IOException e) {
			System.err.println("Log: could not be written to file!");
			e.printStackTrace();
		}
		 
	 }
	 
	 /* Log debug */
	 public static void debug(String debug_str)
	 {
		 if(S2U.DEBUGGING)
		 {
			// write to a log file
			 try {
				 fos.write(debug_str.getBytes());
		 	} catch (IOException e) {
				System.err.println("Fail (DEBUG): writing to a file!!");
				e.printStackTrace();
			}
		 }
	 }
}
