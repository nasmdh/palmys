package sim;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

public class SOList {

	public static List<String> parseSortedOrderList(String filepath)  {
		  List<String> soList = new ArrayList<String>();
		  int parent_index = 0;
		  
		  
		  //read sorted order list
		  Charset charset = Charset.forName("ISO-8859-1");
		  Path path = Paths.get(filepath);
		  List<String> lines = null;
		  String block_son;
		  String block_path; 
		  Pattern slist_pattern = Pattern.compile("([0-9])+:([0-9])+");
		  Pattern subystem_pattern = Pattern.compile("---- Sorted");
		  int scopecount=0, subsystemcount=0;
		  /*soList.add("nas");
		  soList.add("elham");
		  soList.add("mom");
		  
		  System.out.println(soList);
		  */
		try {
			lines = Files.readAllLines(path, Charset.defaultCharset());
			
			  
		  for (String l : lines) {
			
			//check of empty line
			  if(l.isEmpty()){
				//System.out.println("Empty line!");
				continue;
			}
			//check atomic block
			  l=l.trim();
			  Matcher m = slist_pattern.matcher(l);
			  if(m.find() == true){
				  String m_str = m.group(0);
				  String lsplit[] = l.split("'");
				  block_path = lsplit[1];
				  block_son = m_str;
				  if(lsplit[2].contains("SubSystem") == true){
					 // System.out.println(block_path + "\t" + lsplit[2]);
					  ++subsystemcount;
					  continue;
				  }
				  if(lsplit[2].contains("Scope") == true){
					  System.out.println(block_path + "\t" + lsplit[2]);
					  ++scopecount;
					  continue;
				  }
					  
					  
					  
				  soList.add(parent_index++, block_path);
				  //System.out.println("atomic block path: " + block_path);
				 // System.out.println(soList.get(0));
				  continue;
			  }
			  m = subystem_pattern.matcher(l);
			  if(m.find() == true){
				  
				  
				  String lsplit[] = l.split("'");
				  block_path = lsplit[1];
				// System.out.println("subsystem block path: " + block_path);
				 int i = soList.indexOf(block_path);
				 if(i != -1){
					 parent_index = i;
					 soList.remove(parent_index);
				 }
				 
				 //System.out.println(soList.size());
				  continue;
			  }
		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("file not found!");
			e.printStackTrace();
		}
System.out.println("Scopecount: " + scopecount + "\t" + "TmpAtomicSubsys: " + subsystemcount);
		//System.out.println(soList.get(0));
		
		return soList;
		/*
		try(FileWriter fw = new FileWriter("slist_out");
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
			{
			    for (Object s : soList) {
					//System.out.println(s);
					out.println(s);
				}
			//out.println("first");
			//out.println("second");
			} catch (IOException e) {
				System.out.println(e);
			    //exception handling left as an exercise for the reader
			}*/
		
	}
	
	

}
