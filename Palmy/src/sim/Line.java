package sim;

import java.util.HashMap;

public class Line 
{
	Node parent;
	private Block srcblk;
	private String srcport;
	private Block dstblk;
	private String dstport;
	private HashMap<String, String> para_hm = new HashMap<String, String>();
	
	public Line(Node parent, HashMap<String, String> para_hm) 
	{
		this.parent = parent;
		this.para_hm.putAll(para_hm);
		this.srcblk = this.parent.getBlock(para_hm.get("SrcBlock"));
		this.srcport = para_hm.get("SrcPort");
	}
	public Line(Node parent, Block srcblk, String srcport, Block dstblk, String dstport)
	{
		this.parent = parent;
		this.srcblk = srcblk;
		this.dstblk = dstblk;
		this.srcport = srcport;
		this.dstport = dstport;
	}
	
	public void setDstBlock() 
	{ 
		this.dstblk = this.parent.getBlock(para_hm.get("DstBlock")); 
	}
	
	public void setDstPort() 
	{ 
		this.dstport = para_hm.get("DstPort"); 
	}
	
	/* update */
	public void updateSrcBlock(Block blk) { this.srcblk = blk; }
	public void updateDstBlock(Block blk) { this.dstblk = blk; }	
	
	public HashMap<String, String> getParas()
	{
		return this.para_hm;
	}
	
	public Block getSrcBlock()
	{
		return this.srcblk;
	}
	public String getSrcPort()
	{
		return this.srcport;
	}
	
	public Block getDstBlock()
	{
		return this.dstblk;
	}
	public String getDstPort()
	{
		return this.dstport;
	}
	
	public Node getParent() { return this.parent; }
	
}
