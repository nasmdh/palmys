package sim;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {
	
	/* Id */
	private int id;
	private static int total_count;
	private String name;
	public Node node;
	
	/* Elements */
	protected List<Line> line_list = new ArrayList<Line>();
	protected List<Signal> signal_list = new ArrayList<Signal>();
	protected List<Block> blk_list = new ArrayList<Block>();
	protected List<Node> node_list = new ArrayList<Node>();
	protected ArrayList< HashMap< String,String>> blkdefault_hm_list =  new ArrayList<HashMap< String,String>>();
	
	/* Derived elements */

	protected HashMap< String, Integer> blktype_hs = new HashMap< String, Integer >();
	protected HashMap< String, Set< String>> blkatt_hm = new HashMap< String, Set< String>>();
	
	/* Statistics */
	public int blk_count = 0, line_count = 0, branch_count = 0, subsystem_count = 0, modelref_count = 0, 
			annotation_count = 0, port_count = 0, no_so_count = 0, so_matched = 0, so_not_matched = 0;
	public double parse_time, log_time, uppaalxml_elements_gen_time, uppaalxml_file_gen_time;

	/* Block execution order */
	List<Integer> so_hashed_list =  new ArrayList<Integer>();
	List<String> so_list = new ArrayList<String>();
	
	/* Constructor */
	public Model() {
		this.id = ++total_count;
		this.node = new Node(null,"Root");
	}

	/* Getter and setter functions */
	public List<Line> getLines() { return this.line_list; }
	public List<Signal> getSignals() { return this.signal_list; }
	public List<Block> getBlocks() { return this.blk_list; }
	public List<Node> getNodes() { return this.node_list; }
	public Node getNode(){ return this.node; }
	public int getId() { return this.id; }
	public ArrayList< HashMap< String,String>> getBlockParaDefault() { 	return this.blkdefault_hm_list; }
	
	/* Blocks default parameters */
	public void addBlockParasDefault(HashMap<String,String> para_hm)
	{
		HashMap<String,String> hm = new HashMap<String,String>();
		hm.putAll(para_hm);
		this.blkdefault_hm_list.add(hm);
	}
	
	/* Block types */
	public HashMap<String, Integer> getBlockTypes()
	{
		return this.blktype_hs;
	}
	
	/* Execution order */
	public int getSortedOrderNumber(String s)
	{ 
		for (int i = 0; i < so_list.size(); i++) 
		{
			if(s.equals(so_list.get(i)))
				return i;
		}
		return -1;
	}
	public List<String> getSortedOrderNumbers() { return this.so_list; }

	//public int getSortedOrderNumber(int hc){ return this.so_hashed_list.indexOf(hc); }
	public String getAbsNameSortedOrderNumber(int i) { return this.so_list.get(i); }
	
	/* Add block */
	public void addBlock(Block blk)
	{
		this.blk_list.add(blk);//add block
		addBlockType(blk.getType());//add type and count
		
		/* Accumulate block attributes */
		Set<String> src_set = blk.getParas().keySet(); //add block type attributes
		//Set<String> dst_set = this.blkatt_hm.keySet();
		//dst_set.addAll(src_set);
		this.blkatt_hm.put(blk.getType(), src_set);
	}
	
	/* Add block type and count */
	private void addBlockType(String blktype)
	{
		int j;
 		if( this.blktype_hs.get(blktype) != null )
 		{
 			  j = this.blktype_hs.get(blktype);
 			  this.blktype_hs.put(blktype, ++j);
 		}else
 		{
			this.blktype_hs.put(blktype, 1);
   		}
		
	}
	
	/* Print */
	public void printBlockParasDefauls()
	{
		for (HashMap<String,String> h: this.blkdefault_hm_list) 
		{
			System.out.println(h.get("BlockType"));
		}
	}
	
	/* Add Signal */
	public void addSignal(Signal s)
	{
		this.signal_list.add(s);
	}
	
	/* Get block parameter default */
	public HashMap<String, String> getBlockParaDefault(String blktype)
	{
		String value;
		for(HashMap< String, String> h:this.blkdefault_hm_list) 
		{
		  	if((value = h.get("BlockType")) != null) 
		  	{
		  		if(value.equals(blktype)) 
		  		{
					return h;
				}
			}
		}
		
		return null;
	}

	public void configSortedOrderNumber(String filepath)  
	{

		  int parent_index = 0;
		  
		  
		  //read sorted order list
		  Charset charset = Charset.forName("ISO-8859-1");
		  Path path = Paths.get(filepath);
		  List<String> lines = null;
		  String block_son;
		  String block_path; 
		  Pattern slist_pattern = Pattern.compile("([0-9])+:([0-9])+");
		  Pattern subystem_pattern = Pattern.compile("---- Sorted");
		  int scopecount=0, subsystemcount=0;
	
		try {
				lines = Files.readAllLines(path, Charset.defaultCharset());
			
			  
			  for (String l : lines) {
				
				//check of empty line
				  if(l.isEmpty()){
					//System.out.println("Empty line!");
					continue;
				}
			//check atomic block
			  l=l.trim();
			  Matcher m = slist_pattern.matcher(l);
			  if(m.find() == true){
				  String m_str = m.group(0);
				  String lsplit[] = l.split("'");
				  block_path = lsplit[1];
				  block_son = m_str;
				  
				  if(lsplit[2].contains("SubSystem") == true)
				  {
					 // System.out.println(block_path + "\t" + lsplit[2]);
					  ++subsystemcount;
					  continue;
				  }
				  /*if(lsplit[2].contains("Scope") == true)
				  {
					  System.out.println(block_path + "\t" + lsplit[2]);
					  ++scopecount;
					  continue;
				  }*/
					  
					  
					  
				  so_list.add(parent_index++, block_path);
				  //System.out.println("atomic block path: " + block_path);
				 // System.out.println(soList.get(0));
				  continue;
			  }
			  m = subystem_pattern.matcher(l);
			  if(m.find() == true){
				  
				  
				  String lsplit[] = l.split("'");
				  block_path = lsplit[1];
				// System.out.println("subsystem block path: " + block_path);
				 int i = so_list.indexOf(block_path);
				 if(i != -1){
					 parent_index = i;
					 so_list.remove(parent_index);
				 }
				 
				 //System.out.println(soList.size());
				  continue;
			  }
		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("file not found!");
			e.printStackTrace();
		}
		//System.out.println("Scopecount: " + scopecount + "\t" + "TmpAtomicSubsys: " + subsystemcount);
		//System.out.println(soList.get(0));
		
		/* Calculate Hashcode */
		String log = "";
		for (String s : so_list) 
		{
			log +=s+"\n";
			s = "/".concat(s).trim();
			this.so_hashed_list.add(s.hashCode());
		}
		writeToFile(log, "./output/so_flattened.txt");
	}
	
	/* ------------------------- OPTIMIZATION ------------------------------------------*/
	 /* Optimize the the system model */
	 public void optimize()
	 {	 
		 int portblk_count =0, copying_count = 0, iteration=0, noinput=0, nooutput=0;
		 Signal sig;

		boolean change = true;
	
		while(change)
		{
			portblk_count = 0; noinput=0; nooutput=0;
			change = false;
			//System.out.println("Removing Inport and Outport blocks ......");
			for (Block blk : this.blk_list)
			{
				if(blk.getType().equals("Inport") || blk.getType().equals("Outport"))
				{++portblk_count;
					blk.setStatus(S2U.PASSIVE);
					
					if(!blk.getIndexOfInSignals().isEmpty())
					{	
						if(!blk.getIndexOfOutSignals().isEmpty())
						{
							if(this.signal_list.get(blk.getIndexOfInSignal(0)) != this.signal_list.get(blk.getIndexOfOutSignal(0)) )
							{
								sig = this.signal_list.get(blk.getIndexOfInSignal(0));
								this.signal_list.set(blk.getIndexOfOutSignal(0), sig);
								blk.setStatus(S2U.PASSIVE);
								change = true;
								++copying_count;
							}
								
						}else
						{
							++nooutput;//System.out.format("Inport block %s has NO OUTPUT connection!!%n", blk.getName());
						}	
					}else
					{
						++noinput;
						//System.out.format("Inport block %s has NO INPUT connection!!%n", blk.getName());
					}
				}
				
			}++iteration;
			
		}
		System.out.format("\t\t- total port blocks = %d%n", portblk_count);
		System.out.format("\t\t- total iterations = %d%n", iteration);
		System.out.format("\t\t- In->Out operations on Inport and Outport blocks = %d%n", copying_count);
		System.out.format("\t\t- No input = %s, No output = %s%n", noinput, nooutput);
	}
	 
	 
	 public void optimizeConstantBlock()
	 {
		 if(S2U.OPTIMIZATION_CONSTANT_BLOCK)
		{
			 for (Block blk : blk_list)
				{	
					if(blk.getType().equals("Constant"))
					{			
						blk.setStatus(S2U.PASSIVE);
					}
					
				}
		}
	 }
	 
	/* -------------------- LOGGING ------------------------------------------------*/

	 public void connection()
	 {
		 int refblk_count = 0;int rld = 0, sd = 0, it = 0, mux = 0, demux = 0;
		 int index = 1;
		 HashSet< Signal> signal_hs = new HashSet< Signal>(signal_list);
		 List<String> isignals = new ArrayList<String>();
		 List<String> osignals = new ArrayList<String>();
		 String log = "";
		 String formatter_main = ""
		 		+ "%d. Block Global ID: %d, type: %s, path: %s%n"
		 		+ "\tInput Signals:\t %s%n"
		 		+ "\tOutput Signals:\t %s%n%n";
		 log += "Total number of signal_list " + signal_hs.size() + "\n" +
				 "=====================================================================================================\n";
		 
		for (Block blk : blk_list)
		{	
			if(blk.getStatus() == S2U.ACTIVE)
			{			
				//construct the input signals list by name
				for (Integer i : blk.getIndexOfInSignals()) 
				{
					isignals.add(this.signal_list.get(i).getName());
				}
				//construct the output signals list by name
				for (Integer i : blk.getIndexOfOutSignals()) 
				{
				
					osignals.add(this.signal_list.get(i).getName());
				}
				
				log += String.format(formatter_main, index, blk.getGId(), blk.getType(), blk.getAbsName(),
						Arrays.toString(isignals.toArray()), 
						Arrays.toString(osignals.toArray()));	
				index += 1;
			}
			isignals.clear();
			osignals.clear();
		}
		
	
		writeToFile(log, "./output/connection.txt");
		
	 }
	 
	 public void blockPath() 
	 {		  		
		 int rld = 0,sd = 0,it = 0,mux = 0,demux = 0,refblk_count = 0;
		String log = "";
 		for(Block blk: blk_list)
 		{
 			if(blk.getType().equals("Reference"))
			{
				if(blk.getParas().get("SourceType").equals("Rate Limiter Dynamic"))
				{
					++rld;
				}
				if(blk.getParas().get("SourceType").equals("Saturation Dynamic"))
				{
					++sd;
				}
				if(blk.getParas().get("SourceType").equals("Interval Test"))
				{
					++it;
				}

				++refblk_count;
				//System.out.println("SourceType \t " + blk.getParas().get("SourceType"));
			}
			if(blk.getParas().get("BlockType").equals("Mux"))
			{
				++mux;
			}
			if(blk.getParas().get("BlockType").equals("Demux"))
			{
				++demux;
			}
			
 			
 		  if(blk.getStatus() == S2U.ACTIVE)
 		  {

 			 log += String.format("%d\t%s%n", blk.getOutDataType(), blk.getAbsNameSortedOrderNumber());
 			 //( Type %s, SampleTime %s, CompiledSampleTime %s status %b)%n, blk.getType(),
 		//	blk.getSampleTime(), blk.getCompiledSampleTime(), blk.getStatus()
 		  
 		  }
 		}
 		
 		/*System.out.print("Number of Refence blocks " + refblk_count);
		System.out.format(""
				+ "%n\tInterval Test = %s  "
				+ "%n\tRate Limiter Dynamic %s "
				+ "%n\tSaturation Dynamic %s"
				+ "%n\tDemux %s"
				+ "%n\t%n\tMux %s %n"
				, it, rld, sd, demux, mux);*/
		//System.out.print("SourceType " + refblk_count);
 		writeToFile(log, "./output/block_name.txt");
	 }
	 
	 public void signal()
	 {
		 String log="";
		 log += String.format("#signal_list = %d%n", signal_list.size());
		 //args)// = String.format("Total number of Signals: before %d, after %d%n"
		 		//+ "===================================================%n", signal_list.size(), hs.size());
		 
		for (Signal s : signal_list) 
		{
			 log += String.format("%-20s%s%n",s.name, s.getSrcBlock().getAbsName());
			 
		}
		 
	/*	 log += String.format("%n====================================================%n");
		for (String h : hs) {
			log += String.format("%s%n",h);
		}*/
		writeToFile(log, "./output/signal.txt");
		 
	 }
	 
	 /* write log to a file */
	 private static void writeToFile(String log, String file_name)
	 {
		 OutputStream fos;
		 
		// write to a log file
		 try {
			 fos = new FileOutputStream(new File(file_name));
			 byte[] b = log.getBytes();
			 
			 fos.write(b);
			 fos.close();
	 	} catch (IOException e) {
			System.err.println("Log: could not be written to file!");
			e.printStackTrace();
		}
		 
	 }
	 
	 /* Log block types */
	 public void blockType()
	 {
		 String btypes="";
		 btypes += String.format("# block types = %d%n", blktype_hs.size());
		 btypes += "================================================\n";
		 for (Map.Entry<String, Integer> h : blktype_hs.entrySet())
		{
			 btypes += String.format("%-20s%s%n", h.getKey(), h.getValue());
		}
		 writeToFile(btypes, "./output/block_type.txt");
	 }
	 
	 /* Generate attribute config */
	 public static void attributeValueConfig(HashMap<String, Set<String>> block_para_hm)
	 {

		 String att_log = "Syntax: \n\t[Simulink Block Type]: [attributes list separated by new lines]\n" +
				 "=====================================================================================================\n";
		 
		for (Map.Entry<String, Set<String>> btype : block_para_hm.entrySet())
		{
			//att_log += String.format("BlockType %s%n", btype.getValue()));
			att_log +=String.format("%s:%n", btype.getKey()); 
			for (String att:btype.getValue())
			{
				att_log +=String.format("\t%s%n", att);
		    	//logger.debug(String.format("(%s, %s), %n", p.getKey(), p.getValue()));
			}//logger.debug("\n");
			att_log += "\n";
		}
		writeToFile(att_log, "./output/attribute_config.txt");
		
	 }
	 
	 private void statistics()
	 {
		 
		 
		 String stat="Model Statistics: \n";
		 stat += "================================================\n";
		
		 stat += String.format(" #Subsystems %20s%n #Blocks %20s%n #Lines %20s%n #Branches %20s%n "
		 		+ "#Annotations %20s%n #Ports %20s%n #SO matched %20s%n #SO NOT matched %20s%n", 
				 subsystem_count, blk_count, line_count, branch_count, annotation_count, port_count, so_matched, so_not_matched);
		 
		 writeToFile(stat, "./output/stat.txt");
	 }
	 
		public void log()
		{
			System.out.println("\n= > Model signals!");
			signal();
			
			//System.out.println("\n= > Model Block Types");
		//	S2ULog.blockType(block_type_hm);
		
			System.out.println("\n= > Model connnections (insignals and outsignals of blocks)!");
			connection();
		
			System.out.println("\n= > Block names!");
			blockPath();
	
			System.out.println("\n= > Mode statistics!");
			statistics();
			
			//System.out.println("\n= > Model Block Types Attribute-value config");
			//S2ULog.attributeValueConfig(block_para_hm);
			//GenerateUPPAALXML.soListDecl("./input/so_list.txt");
		
		}
		
}
