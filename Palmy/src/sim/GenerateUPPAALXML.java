package sim;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class GenerateUPPAALXML {
	
	static List<String> processes = new ArrayList<String>();
	static List< String> para_selected=null;
	static String para_decls = "\n\n/*##--------------- PARAMETERS DECLARATIONS -----------------*/\n\n";


	static String process_decls;
	static String sig_decls;
	//String para_decls;
	static String so_decls;	
	static  String block_type;
	static List< String> attributes = new ArrayList<String>();
	static HashMap<String, List<String>> selected_para_hm =
				new HashMap<String, List<String>>();
	
	
	public static HashSet<String> para_not_defined = new HashSet<String>();
	public static HashSet<String> para_defined = new HashSet<String>();
		
	/* read the attribute configure */
	public static void readAttConfig() {
		String blk_type;
		boolean flag = false;//block type has no selected para yet
		try {
			// Construct BufferedReader from FileReader
			BufferedReader br = new BufferedReader(new FileReader("./attribute.cfg"));
			String att[];
			String line = null;
			while ((line = br.readLine()) != null) 
			{
				//System.out.println(line);
				
				//skip # character
				if(line.startsWith("#"))
					continue;
				
				//check for block type
				if(line.lastIndexOf(':') != -1)
				{
					line = line.substring(0, line.length()-1);
					List<String> tmpatt = new ArrayList<String>();

					blk_type = line;
					//System.out.println("length " + line.length());
					while((line = br.readLine()).trim().length() != 0)
					{//System.out.print("A");
								
						att = line.split("\t");
						//System.out.println("attribute " + att[0] + " " + att[1]);
						if(att[0].equals("1"))
						{flag=true;//has selected para
							//System.out.println("attribute " + att[0] + " " + att[1]);
							tmpatt.add(att[1]);
						}
					}
					//if(flag)
					//{
						selected_para_hm.put(blk_type,tmpatt);
					//	flag = false;
					//}
					//selected_para_list.add(tmpatt_hm);
					//tmpatt.clear();
					//selected_para_list.clear();
				}
			}
			//print the selected attributes
				/*for (Map.Entry<String, List<String>> h : selected_para_hm.entrySet()) 
				{
					System.out.print(h.getKey());
					System.out.println(Arrays.toString(h.getValue().toArray()));
				}*/

				br.close();
		} catch (Exception e) {
			System.out.println("File not found!");
		}
		
	}
	
	/* Create parameter declaration */

	public static String createParaDecl(Block blk, List<String> selected_paras)
	{
		String para_decl = "";// = new String();//"double\t PARA_"+this.name+" = {"
		int i = 0;
		String tmp;
		
		   para_decl += String.format("const double\t para_%d[10] = {", blk.getGId());
		   double pd;
		   //double d = 4.3;
			for (Map.Entry<String, String> p : blk.getParas().entrySet()) 
			{
			  
				
			 
				if(selected_paras.contains(p.getKey())) 
				{
					//if(blk.getType().equals("RelationalOperator"))
					//	System.out.println(p.getKey() +" "+ p.getValue());
					//pd = Double.parseDouble(p.getValue());
					//System.out.println(p.getValue());
				  if(p.getValue().matches("[-+]?([0-9]*\\.[0-9]+|[0-9]+)"))
				  {
					  //p.setValue(p.getValue().concat(""))
					 // int pint = Integer.getInteger(p.getValue());
					  pd = Double.parseDouble(p.getValue());
					 // p.setValue(String.format(Locale.US, "%f, ", pd));				
				  }
				  else
				  {	pd=(double)p.getValue().hashCode();
				 
					  //para_decl += String.format("%f", p.getValue());
				  }
				  para_decl += String.format(Locale.US, "%f/*%s*/, ", pd, p.getValue());
					++i;
				}
					
			}	
			while(i++ != 10) 
			{
				para_decl = para_decl+0.0+", ";
			}
			para_decl = para_decl.substring(0,para_decl.length()-2);
			para_decl = para_decl + " };";
			
			para_decl =  para_decl.replace("<", "lt");
			//para_decl =  para_decl.replace(">", "gt");
			//para_decl =  para_decl.replace(">=", "gte");
			para_decl =  para_decl.replace("<=", "lte");

		return para_decl;
	}
	
	/* UPPAAL XML Generation Related Methods */
	public static String processDecl(List<Block> blk_list, List<Signal> signals)
	{
		int count = 0;
		String log =  "\n/*\n##--------------- PROCESS DECLARATIONS -----------------\n*/\n";
		String process = "";
		String function = "";
		String isig_str="", osig_str="";
		double st;
		
		for (Block blk: blk_list)
		{
			if(blk.getStatus() == S2U.ACTIVE)
			{++count;
			/* Construct input signals */
			if(!blk.getIndexOfInSignals().isEmpty())
			{
				for (Signal s : blk.getInSignals(signals)) 
				{
					isig_str+=",".concat(s.getName());
					if(s.getDataType() == S2U.VECTOR)
						isig_str += "_v";
					else
						isig_str += "_s";
				}
			}
			/* Construct output signals */
			if(!blk.getIndexOfOutSignals().isEmpty())
			{
				for (Signal s : blk.getOutSignals(signals)) 
				{
					osig_str+=",".concat(s.getName());
					if(s.getDataType() == S2U.VECTOR)
						osig_str += "_v";
					else
						osig_str += "_s";
				}
			}
			
			st = Double.parseDouble(blk.getCompiledSampleTime());
			

				process = String.format("process_%s", blk.getGId());
				
				function = String.format("%s%d%d",  blk.getType(), blk.getIndexOfInSignals().size(),blk.getIndexOfOutSignals().size());
				if(blk.getOutDataType() == S2U.VECTOR)
					function += "_v";
				
				
				log += String.format(Locale.US,"%s = %s(para_%s %s %s, %f, so_%s);%n", 
						process, function, blk.getGId(), isig_str, osig_str, st, blk.getGId());
				log += String.format("/* %s */%n", blk.getAbsName());
				
				processes.add(process);
			}
			isig_str = "";osig_str="";
		}

		log += String.format("%n/*%n## number = %d%n"
				+ "##-------------------------------------------------------%n*/%n", count);
		
		//System.out.print(log);
		log += sysDecl();
		
		return log;
	}
	
	/* UPPAAL XML Generation Related Methods */
	public static String paraDecl(List<Block> blk_list)
	{
		int count = 0;
		//Block blk;
		String log =  "\n/*##--------------- PARAMETER DECLARATIONS -----------------*/\n";

		String para_decl = "";
		
		for (Block blk: blk_list)
		{
			if(blk.getStatus() == S2U.ACTIVE)
			{ ++count;
				List<String> selected_para_list = selected_para_hm.get(blk.getType());
				
				
				if(selected_para_list != null)
				{
					//System.out.println(Arrays.toString(selected_para_list.toArray()));
					para_decl += createParaDecl(blk, selected_para_list);
					para_decl += String.format("%n/* BlockType: %s, %s*/%n", 
							 blk.getType(), Arrays.toString(selected_para_list.toArray()));
					
					
					
				}else 
				{
					para_decl += String.format("/*Not defined selected parameters for block type: %s*/%n", blk.getType());	
					para_not_defined.add(blk.getType());
					continue;
				}
			 // para_decl +=String.format("/* block type: %s */%n", blk.getType());
			  
			  para_defined.add(blk.getType());
			}
		}
		//System.out.print(log);
		log += para_decl;
		
		log += String.format("%n/*%n## number = %d%n"
				+ "##-------------------------------------------------------%n*/%n", count);
		
		return log;
	}
	public static String signalDecl(List<Signal> sl)
	{
		String log = "\n/*\n##--------------- SIGNAL DECLARATIONS -----------------\n";
		log += String.format("## Number of signals (Before Optimization) = %d %n", sl.size());
		HashSet<Signal> signal_hs = new HashSet< Signal>(sl);
		log += String.format("## Number of signals (After Optimization) = %d%n "
				+ "##-------------------------------------------------------%n*/%n", signal_hs.size());
		String str;
		
		//for(Signal s:sl) 
		//{
		for (Signal s : signal_hs) 
		{
			
			if(S2U.OPTIMIZATION_CONSTANT_BLOCK)
			{
				if(s.getSrcBlock().getType().equals("Constant"))
				{str = s.getSrcBlock().getParas().get("Value");
					 if(str.matches("[-+]?([0-9]*\\.[0-9]+|[0-9]+)"))
					  {
						  double pd = Double.parseDouble(s.getSrcBlock().getParas().get("Value"));		
						  log += String.format(Locale.US,"%ndouble\t%s_s = %f;", s.name, pd);
					  }else 
					  {
						  if(str.trim().equals("false"))
							  log += String.format("%ndouble\t%s_s = %s;", s.name, "FALSE");
						  else if(str.trim().equals("true"))
							  log += String.format("%ndouble\t%s_s = %s;", s.name, "TRUE");
						  else
							  log += String.format("%ndouble\t%s_s = %s;", s.name, str);
						  
						//  System.out.println(str);
					  }
				}else
				{
					if(s.datatype == S2U.VECTOR)
						log += String.format("%ndouble\t%s_v[10] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };", s.name);
					else
						log += String.format("%ndouble\t%s_s = 0.0;", s.name);
				}
			}else
				
			{
				if(s.datatype == S2U.VECTOR)
					log += String.format("%ndouble\t%s_v[10] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };", s.name);
				else
					log += String.format("%ndouble\t%s_s = 0.0;", s.name);
			}
		}
		//System.out.println(log);
		
		return log;
	}
	
	public static String signalDeclx(List<Block> blk_list, List<Signal> signals)
	{
		String log = "\n/*\n##--------------- SIGNAL DECLARATIONS -----------------\n";
		//log += String.format("## Number of signals (Before Optimization) = %d %n", sl.size());
		//HashSet<Signal> signal_hs = new HashSet< Signal>(sl);
		//log += String.format("## Number of signals (After Optimization) = %d%n "
		//		+ "##-------------------------------------------------------%n*/%n", signal_hs.size());
		
		
		for(Block blk:blk_list) 
		{
			if(blk.out_datatype == S2U.VECTOR)
			{
				for (Integer i : blk.getIndexOfOutSignals()) 
				{
					log += String.format("%ndouble\t%s[10] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };", signals.get(i).name);
				}
				
			}else
			{
				for (Integer i : blk.getIndexOfOutSignals()) 
				{
					log += String.format("%ndouble\t%s = 0.0;", signals.get(i).name);
				}
				
			}
		}
		//System.out.println(log);
		
		return log;
	}
	
	 private static String sysDecl()
	 {
		 String sys_decl = "\n\n/*##--------------- SYSTEM DECLARATION -----------------*/\n\n";

		 sys_decl += "system " + processes.get(0);
		 
		for (int i=1; i < processes.size();  ++i)
		{
			sys_decl += String.format(", %n%s", processes.get(i));
		}sys_decl += ";";
		
		
		return sys_decl;
	 }
	 
	public static String soListDecl(List<Block> blk_list)
	{
		String log = "/*\n##--------------- SORTED ORDER LIST DECLARATIONS -----------------\n";
	//	HashMap<Integer, String> so_ordered_hm= new HashMap<Integer, String>();
		List<Block> sorted_blk_list= new ArrayList<Block>();
		int i;
		
		log += String.format("## number = %d%n"
				+ "##-------------------------------------------------------%n*/%n",
				blk_list.size());
		for(Block blk:blk_list) 
		{
			if(blk.getStatus() == S2U.ACTIVE)
			{
				if(sorted_blk_list.isEmpty())
				{
					sorted_blk_list.add(blk);
				}else
				{
					for (i = 0; i < sorted_blk_list.size(); i++) {
						if(blk.getSorderNumber() < sorted_blk_list.get(i).getSorderNumber())
						{
							break;
						}
					}
					sorted_blk_list.add(i, blk);
					
				}
			}	
		}

		i = 0;
		for(Block blk:sorted_blk_list) 
		{
		log += String.format(Locale.US, "%nconst int\tso_%s = %d;", blk.getGId(), i++);
		}
		
		return log;
	}
	
	public static void createElement(Model m)
	{  
		/*System.out.println("\n\n/******************************* GNERATION OF UPPAAL XML FILE ************************");
		System.out.println("*");
		System.out.println("*");
		System.out.println("**************************************************************************************///");
		
		/*------------------CREATE UPPAAL XML ELEMENTS -----------------*/
		
		GenerateUPPAALXML.readAttConfig();
		process_decls = processDecl(m.getBlocks(), m.getSignals());
		sig_decls 	 = signalDecl(m.getSignals());
		para_decls 	 = paraDecl(m.getBlocks());
		so_decls 	= soListDecl(m.getBlocks());
		
		
		
     /*   System.out.format("Block types whose paras defined (#blockes types: %d)%n", GenerateUPPAALXML.para_defined.size());
        for (String hs : GenerateUPPAALXML.para_defined) {
                System.out.println(hs);
        }
		System.out.format("Block types whose paras not defined yet (#blockes types: %d)%n", GenerateUPPAALXML.para_not_defined.size());
	  	for (String hs : GenerateUPPAALXML.para_not_defined) {
			System.out.println(hs);
		}*/
	//	System.out.println("\n::Preparing UPPAAL XML elements: ...COMPLETED!!\n");
	//end of creating UPPAAL XML elements
	  
	}

	public static void file(String ifile_path, String ofile_path)
	{
		FileInputStream fis = null;
		FileOutputStream fop = null;
		File ifile, ofile;

		try {//System.out.println(file_path);
		  	ifile = new File(ifile_path);
			fis = new FileInputStream(ifile);
			ofile = new File(ofile_path);
			fop = new FileOutputStream(ofile);

			//System.out.println("Total file size to read (in bytes) : "
			//		+ fis.available());
			// if file doesnt exists, then create it
			if (!ofile.exists()) {
				ofile.createNewFile();
			}
			
			int i;
			boolean OPENING_SYS_MACHED = false, CLOSING_SYS_MACHED = false;
			while ((i = fis.read()) != -1) {

				if(!OPENING_SYS_MACHED) {
				  	fop.write(i);
				  	if((char)i == '<') { 
				  		i = fis.read(); fop.write(i);
					if((char)i == 's') { 
				  		i = fis.read();fop.write(i);
					if((char)i == 'y') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 's') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 't') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 'e') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 'm') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == '>') {
			  		  OPENING_SYS_MACHED = true;
			  			//System.out.println("<system> found!!");//i = fis.read()
						byte[] process_decls_bytes = process_decls.getBytes();
						byte[] para_decls_bytes = para_decls.getBytes();
						byte[] sig_decls_bytes = sig_decls.getBytes();
						byte[] so_decls_bytes = so_decls.getBytes();

						fop.write(so_decls_bytes);
						fop.write(sig_decls_bytes);
						fop.write(para_decls_bytes);
						fop.write(process_decls_bytes);
						continue;
			  				
					}}}}}}}}
				}

				if(OPENING_SYS_MACHED) { 
				if(!CLOSING_SYS_MACHED) {
				 /* i = fis.read(); fop.write(i);// <
				  i = fis.read(); fop.write(i);// /
				  i = fis.read(); fop.write(i);// s
				  i = fis.read(); fop.write(i);// y
				  i = fis.read(); fop.write(i);// s 
				  i = fis.read(); fop.write(i);// t
				  i = fis.read(); fop.write(i);// e
				  i = fis.read(); fop.write(i);// m
				  i = fis.read(); 				// > - to be written next*/
				  	fop.write(i);
				  	if((char)i == '<') { 
				  		i = fis.read(); fop.write(i);
				  	if((char)i == '/') { 
				  		i = fis.read(); fop.write(i);
					if((char)i == 's') { 
				  		i = fis.read();fop.write(i);
					if((char)i == 'y') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 's') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 't') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 'e') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == 'm') { 
			  			i = fis.read();fop.write(i);
			  		if((char)i == '>') {
			  		  CLOSING_SYS_MACHED = true;
			  			//System.out.println("</system> found!!");//i = fis.read()
			  			i = fis.read();
			  				
					}}}}}}}}}
				}
				}
				
				if(CLOSING_SYS_MACHED && OPENING_SYS_MACHED)
				{
				  fop.write(i);
				}
				//fop.write(i);
				// convert to char and display it
				//if()System.out.print((char) content);
				
			}
		fis.close();
		fop.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}  

	   public static void createFile(String ifile_path, String ofile_path){

	      try {	
	         File inputFile = new File(ifile_path);
	         DocumentBuilderFactory dbFactory 
	            = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         System.out.println("\nTemplate Root element :" 
	            + doc.getDocumentElement().getNodeName());
	         NodeList temp_list = doc.getElementsByTagName("system");
	         temp_list.item(0).setTextContent(String.format("%s%s%s%s", so_decls, sig_decls, para_decls,process_decls));
	         
	      // write the content into xml file
	 		TransformerFactory transformerFactory = TransformerFactory.newInstance();
	 		Transformer transformer = transformerFactory.newTransformer();
	 		DOMSource source = new DOMSource(doc);
	 		StreamResult result = new StreamResult(new File(ofile_path));
	 		transformer.transform(source, result);
	 
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }
	

}
