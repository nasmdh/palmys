package sim;

public class Port {
	
	private int id;
	private int gid;
	private String datatype;
	
	public Port(int id, int gid, String datatype) // id = port number
	{
		this.id = id;
		this.gid = gid;
		this.datatype = datatype;
	}
	
	public int getPort() {return this.id;}
	public int getGId() {return this.gid;}
	public String getDataType() {return this.datatype;}
}
